import React, { useState } from "react";
import logo from "../../favicon.png";
import { useHistory } from "react-router-dom";
import { Link } from "react-router-dom";
import { LoginOutlined } from "@ant-design/icons";
import { Badge } from "antd";

const Search = ({ setUserInfo, CartItem, clearCart }) => {
  const [search, setSearch] = useState("");
  let history = useHistory();

  const handleKeydown = (key) => {
    if (key.key === "Enter") {
      history.push(`/market?search=${search}`);
    }
  };

  return (
    <>
      <section className="search">
        <div className="container c_flex">
          <div className="logo width ">
            <img src={logo} alt="" />
          </div>

          {/* Search input for Market Dashboard */}
          <div className="search-box f_flex">
            <i className="fa fa-search"></i>
            <input
              type="text"
              placeholder="Search and hit enter..."
              onKeyDown={handleKeydown}
              onChange={(e) => setSearch(e.target.value)}
            />
          </div>

          {/* Navigatio */}
          <div
            style={{
              display: "flex",
              flexDirection: "row",
              width: "35%",
              justifyContent: "space-between",
            }}
          >
            <li>
              {/* Redirecting to Market page */}
              <Link to="/market">
                <h3>HOME</h3>
              </Link>
            </li>
            <li>
              {/* Redirecting to User profile page */}
              <Link to="/profile">
                <h3>USER ACCOUNT</h3>
              </Link>
            </li>
            <li>
              <Badge count={CartItem.length}>
                {/* Redirecting to Cart page */}
                <Link to="/cart">
                  <h3 style={{ width: 50 }}>CART</h3>
                </Link>
              </Badge>
            </li>

            {/* Logout Button */}
            <li>
              <div
                style={{
                  cursor: "pointer",
                }}
                onClick={() => {
                  clearCart();
                  setUserInfo(null);
                  localStorage.clear();
                  history.replace("/signin");
                }}
              >
                <LoginOutlined />
              </div>
            </li>
          </div>
        </div>
      </section>
    </>
  );
};

export default Search;
