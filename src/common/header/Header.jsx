import React from "react";
import "./Header.css";
import Head from "./Head";
import Search from "./Search";

const Header = ({ CartItem, setUserInfo, clearCart }) => {
  return (
    <div style={{ background: "#fff" }}>
      <Head />
      <Search
        setUserInfo={setUserInfo}
        CartItem={CartItem}
        clearCart={clearCart}
      />
    </div>
  );
};

export default Header;
