import React from "react";
import Header from "../header/Header";
import Footer from "../footer/Footer";

const HeaderFooter = ({ children, CartItem, setUserInfo, clearCart }) => {
  return (
    <div style={{ backgroundColor: "#F3F3F3" }}>
      <Header
        setUserInfo={setUserInfo}
        CartItem={CartItem}
        clearCart={clearCart}
      />
      {children}
      <Footer />
    </div>
  );
};

export default HeaderFooter;
