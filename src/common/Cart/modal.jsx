import React, { useEffect, useState } from "react";
import { Modal, Button } from "antd";
import { QuestionCircleTwoTone } from "@ant-design/icons";
import "./style.css";

const ConfirmModal = ({ handleBuyItems, disable }) => {
  const [visible, setVisible] = useState(false);

  useEffect(() => {}, []);

  const showModal = () => {
    setVisible(true);
  };
  const handleCancel = () => {
    setVisible(false);
  };
  const handleConfirm = () => {
    handleBuyItems();
    setVisible(false);
  };

  return (
    <>
      <Button
        disabled={disable}
        style={{ width: "100%" }}
        type="primary"
        size={"large"}
        onClick={showModal}
      >
        Buy
      </Button>

      {visible ? (
        <Modal
          style={{ width: "500px" }}
          visible={visible}
          footer={null}
          onCancel={handleCancel}
          closable={false}
        >
          <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "column",
              padding: "30px",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <QuestionCircleTwoTone
              twoToneColor="#52c41a"
              style={{
                fontSize: "50px",
                marginBottom: "10px",
              }}
            />

            <p
              style={{
                fontSize: "18px",
              }}
            >
              Buy this item(s)?
            </p>
            <div
              style={{
                marginTop: "20px",
                width: "250px",
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Button
                type="primary"
                style={{ width: "100px" }}
                onClick={handleConfirm}
              >
                Confirm
              </Button>
              <Button onClick={handleCancel} style={{ width: "100px" }}>
                Cancel
              </Button>
            </div>
          </div>
        </Modal>
      ) : null}
    </>
  );
};

export default ConfirmModal;
