import React from "react";
import "./style.css";

const Footer = () => {
  return (
    <>
      <footer>
        <div className="container grid2">
          <div className="box">
            <h1>Seafood</h1>
            <p>
              Live seafood up for grabs! We offer gifts from the sea. Your fresh
              catch market destination! We’re the freshest source you can find.
              We have all your seafood cravings.
            </p>
            <div className="icon d_flex">
              <div className="img d_flex">
                <i class="fa-brands fa-google-play"></i>
                <span>Google Play</span>
              </div>
              <div className="img d_flex">
                <i class="fa-brands fa-app-store-ios"></i>
                <span>App Store</span>
              </div>
            </div>
          </div>

          <div className="box">
            <h2 style={{ color: "#fff" }}>About Us</h2>
            <ul>
              <li>Careers</li>
              <li>Our Stores</li>
              <li>Our Cares</li>
              <li>Terms & Conditions</li>
              <li>Privacy Policy</li>
            </ul>
          </div>
          <div className="box">
            <h2 style={{ color: "#fff" }}>Customer Care</h2>
            <ul>
              <li>Help Center </li>
              <li>How to Buy </li>
              <li>Track Your Order </li>
              <li>Returns & Refunds </li>
            </ul>
          </div>
          <div className="box">
            <h2 style={{ color: "#fff" }}>Contact Us</h2>
            <ul>
              <li>Pag-asa St, Caniogan, Pasig City </li>
              <li>Email: group5it224@gmail.com</li>
              <li>Phone: +0999 999 9999</li>
            </ul>
          </div>
        </div>
      </footer>
    </>
  );
};

export default Footer;
