import React, { useEffect, useState } from "react";
import { Divider, Modal, message, Row, Col } from "antd";
import { CloseOutlined } from "@ant-design/icons";
import "./style.css";
import moment from "moment";
import axios from "axios";

const initialCustomerInfo = {
  imageUrl: "",
  firstName: "",
  lastName: "",
  username: "",
  role: "",
  password: "",
  address: "",
  contact: "",
  age: "",
  birthDate: "",
  gender: "",
  id: "",
};

const CommonModal = ({ record, view, handleCancel }) => {
  const _baseUrl = process.env.REACT_APP_BASE_URL;
  const [customerInfo, setCustomerInfo] = useState(initialCustomerInfo);

  useEffect(() => {}, []);

  const requestCustomerInfo = async () => {
    try {
      const res = await axios.get(`${_baseUrl}/users/${record.customerId}`);
      setCustomerInfo(res.data);
    } catch (error) {
      message.error("Something went wrong. Please reload!");
    }
  };

  useEffect(() => {
    if (record != null) {
      requestCustomerInfo();
      return;
    } else {
      setCustomerInfo(initialCustomerInfo);
    }
  }, [record]);

  return (
    <>
      {view ? (
        <Modal
          style={{ width: "500px" }}
          visible={view}
          footer={null}
          onCancel={handleCancel}
          closable={false}
        >
          <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "column",
              padding: "30px",
            }}
          >
            <CloseOutlined
              onClick={handleCancel}
              style={{
                position: "absolute",
                top: "-30px",
                right: "-30px",
                color: "#fff",
              }}
            />
            <div
              style={{
                width: "200px",
                height: "200px",
                backgroundImage: `url(${record.imageUrl})`,
                backgroundRepeat: "no-repeat",
                backgroundSize: "contain",
                backgroundPosition: "center",
                alignSelf: "center",
              }}
            />
            <p>
              <b>Product</b>: {record.productName}
            </p>
            <p>
              <b>Date</b>: {moment(record.date).format("L")}
            </p>
            <Row>
              <Col span={12}>
                <p>
                  <b>Price</b>: {record.price}
                </p>
              </Col>
              <Col span={12}>
                <p>
                  <b>Quantity</b>: {record.quantity}
                </p>
              </Col>
            </Row>
            <Divider />
            <p>
              <b>Customer Name</b>:{" "}
              {`${customerInfo.firstName} ${customerInfo.lastName}`}
            </p>
            <p>
              <b>Address:</b>: {customerInfo.address}
            </p>
            <p>
              <b>Contact</b>: {customerInfo.contact}
            </p>
          </div>
        </Modal>
      ) : null}
    </>
  );
};

export default CommonModal;
