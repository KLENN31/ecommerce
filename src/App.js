import React, { useState } from "react";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import HeaderFooter from "./common/template/HeaderFooter";
import Pages from "./pages/Pages";
import Cart from "./common/Cart/Cart";
import Signin from "./pages/Signin";
import Signup from "./pages/Signup";
import ItemDetails from "./pages/itemDetails";
import Profile from "./pages/profile";
import BackOffice from "./pages/backoffice";

function App() {
  const storedUser = localStorage.getItem("user");
  const storedCart = localStorage.getItem("cart");
  const [CartItem, setCartItem] = useState(
    storedCart !== undefined && storedCart !== null
      ? JSON.parse(storedCart)
      : []
  );
  const [UserInfo, setUserInfo] = useState(
    storedUser !== undefined && storedUser !== null
      ? JSON.parse(storedUser)
      : null
  );

  const addToCart = (product) => {
    const productExit = CartItem.find((item) => item.id === product.id);
    let newList = [];
    if (productExit) {
      newList = CartItem.map((item) =>
        item.id === product.id
          ? { ...productExit, quantity: productExit.quantity + 1 }
          : item
      );
      localStorage.setItem("cart", JSON.stringify(newList));
    } else {
      newList = [...CartItem, { ...product, quantity: 1 }];
    }
    setCartItem(newList);
    localStorage.setItem("cart", JSON.stringify(newList));
  };

  const decreaseQty = (product) => {
    const productExit = CartItem.find((item) => item.id === product.id);
    let newList = [];

    if (productExit.quantity === 1) {
      newList = CartItem.filter((item) => item.id !== product.id);
    } else {
      newList = CartItem.map((item) =>
        item.id === product.id
          ? { ...productExit, quantity: productExit.quantity - 1 }
          : item
      );
    }
    setCartItem(newList);
    localStorage.setItem("cart", JSON.stringify(newList));
  };

  const clearCart = () => {
    setCartItem([]);
    localStorage.setItem("cart", JSON.stringify([]));
  };

  const removeItemToCart = (productId) => {
    let newList = CartItem.filter((value) => {
      return value.id !== productId;
    });
    setCartItem(newList);
    localStorage.setItem("cart", JSON.stringify(newList));
  };

  return (
    <>
      <Router>
        <Switch>
          <Route path={"/signin"} exact>
            <Signin setUserInfo={setUserInfo} />
          </Route>
          <Route path="/signup" exact>
            <Signup />
          </Route>
          {UserInfo !== null && UserInfo.role === "customer"
            ? [
                /// Customer Routes
                <Route path="/cart" exact>
                  <HeaderFooter
                    setUserInfo={setUserInfo}
                    CartItem={CartItem}
                    clearCart={clearCart}
                  >
                    <Cart
                      CartItem={CartItem}
                      UserInfo={UserInfo}
                      addToCart={addToCart}
                      decreaseQty={decreaseQty}
                      clearCart={clearCart}
                      removeItemToCart={removeItemToCart}
                    />
                  </HeaderFooter>
                </Route>,
                <Route path="/item-details/:id" exact>
                  <HeaderFooter
                    setUserInfo={setUserInfo}
                    CartItem={CartItem}
                    clearCart={clearCart}
                  >
                    <ItemDetails addToCart={addToCart} />
                  </HeaderFooter>
                </Route>,
                <Route path="/profile" exact>
                  <HeaderFooter
                    setUserInfo={setUserInfo}
                    CartItem={CartItem}
                    clearCart={clearCart}
                  >
                    <Profile profile={UserInfo} updateUser={setUserInfo} />
                  </HeaderFooter>
                </Route>,
                <Route path={["/market"]}>
                  <HeaderFooter
                    setUserInfo={setUserInfo}
                    CartItem={CartItem}
                    clearCart={clearCart}
                  >
                    <Pages addToCart={addToCart} />
                  </HeaderFooter>
                </Route>,
              ]
            : null}
          {UserInfo !== null && UserInfo.role === "employee"
            ? [
                /// Employee Routes
                <Route path="/back-office" exact>
                  <BackOffice setUserInfo={setUserInfo} />
                </Route>,
              ]
            : null}
          <Redirect to="/signin" />
        </Switch>
      </Router>
    </>
  );
}

export default App;
