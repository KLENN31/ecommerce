import React from "react";
import Catg from "./Catg";
import ShopCart from "./ShopCart";
import "./style.css";

const Shop = ({ addToCart }) => {
  return (
    <>
      <section className="shop background">
        <div
          style={{
            display: "flex",
            flexDirection: "row",
            maxWidth: "95vw",
            padding: "30px",
          }}
        >
          {/* Category components */}
          <Catg />

          {/* Product List component */}
          <ShopCart addToCart={addToCart} />
        </div>
      </section>
    </>
  );
};

export default Shop;
