import React from "react";
import { useHistory } from "react-router-dom";
import _ from "lodash";

const Catg = () => {
  const location = useHistory();
  const data = [
    {
      cateImg: "./images/category/shop-1.png",
      cateName: "Fish",
    },
    {
      cateImg: "./images/category/shop-2.png",
      cateName: "Shrimp",
    },
    {
      cateImg: "./images/category/shop-3.png",
      cateName: "Shellfish ",
    },
    {
      cateImg: "./images/category/shop-4.png",
      cateName: "Squid",
    },
  ];
  return (
    <>
      <div className="category" style={{ marginTop: "-30px" }}>
        <div className="chead d_flex">
          <h1>Category</h1>
        </div>
        {data.map(({ cateName }, index) => {
          return (
            <a
              onClick={() =>
                location.push(`/market?category=${_.lowerCase(cateName)}`)
              }
              key={`category-${index}`}
            >
              <div className="box f_flex" key={index}>
                <span>{cateName}</span>
              </div>
            </a>
          );
        })}
      </div>
    </>
  );
};

export default Catg;
