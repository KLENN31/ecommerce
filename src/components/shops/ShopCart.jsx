import React, { useState, useEffect } from "react";
import axios from "axios";
import { Link, useLocation } from "react-router-dom";
import { Col, Divider, Row } from "antd";

const ShopCart = ({ addToCart }) => {
  const _baseUrl = process.env.REACT_APP_BASE_URL;
  const [productList, setProductList] = useState([]);
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const searchParam = searchParams.get("search");
  const categoryParam = searchParams.get("category");

  // Check update filter
  useEffect(() => {
    let params = "";

    if (
      categoryParam != null &&
      categoryParam != undefined &&
      categoryParam != ""
    ) {
      params += "category=" + categoryParam;
    }
    if (searchParam != null && searchParam != undefined && searchParam != "") {
      params += "productName_like=" + searchParam;
    }
    // Request Database product list
    axios.get(`${_baseUrl}/product?${params}`).then((res) => {
      setProductList(res.data);
    });
  }, [searchParam, categoryParam]);

  return (
    <>
      <Row style={{ marginTop: "-40px" }}>
        {/* Render Market List */}
        {productList.map((item, index) => (
          <Col className="gutter-row" key={`item-${index}`}>
            <div style={{ width: "270px" }} className="product mtop">
              <Link className="box" to={`/item-details/${item.id}`}>
                <div className="img">
                  <div
                    style={{
                      width: "100%",
                      display: "flex",
                      justifyContent: "center",
                    }}
                  >
                    <div
                      style={{
                        width: "180px",
                        height: "180px",
                        backgroundImage: `url(${item.imageUrl})`,
                        backgroundRepeat: "no-repeat",
                        backgroundSize: "contain",
                        backgroundPosition: "center",
                        alignSelf: "center",
                      }}
                    />
                  </div>
                  <Divider />

                  <div className="product-like"></div>
                </div>
              </Link>
              <div className="product-details">
                <h3>
                  <b style={{ textTransform: "capitalize" }}>
                    {item.productName}
                  </b>
                </h3>
                <div className="rate">
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                  <i className="fa fa-star"></i>
                </div>
                <div
                  className="price"
                  style={{
                    display: "flex",
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}
                >
                  <h4>₱{item.price}.00 </h4>
                  <button
                    style={{ marginTop: "-15px" }}
                    onClick={() => addToCart(item)}
                  >
                    <i className="fa fa-plus"></i>
                  </button>
                </div>
              </div>
            </div>
          </Col>
        ))}
      </Row>
    </>
  );
};

export default ShopCart;
