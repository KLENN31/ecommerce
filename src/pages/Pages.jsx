import React from "react";
import Shop from "../components/shops/Shop";
import Wrapper from "../components/wrapper/Wrapper";

const Pages = ({ addToCart }) => {
  return (
    <>
      <Shop addToCart={addToCart} />
      <Wrapper />
    </>
  );
};

export default Pages;
