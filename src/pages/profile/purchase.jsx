import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { Col, message, Row } from "antd";
import axios from "axios";
import "./style.css";

import Delete from "./delete";

const Purchase = ({ profile }) => {
  const _baseUrl = process.env.REACT_APP_BASE_URL;
  const [active, setActive] = useState("onGoing");
  const [loading, setLoading] = useState(false);
  const [list, setList] = useState([]);

  const data = [1, 2];

  let {} = useParams();
  useEffect(() => {
    getPurchaseList();
  }, []);

  useEffect(() => {
    getPurchaseList();
  }, [active]);

  const getPurchaseList = async () => {
    setLoading(true);

    try {
      let listType = "ongoingDeliveries";

      switch (active) {
        case "onGoing":
          listType = "ongoingDeliveries";
          break;
        case "complete":
          listType = "deliveredProduct";
          break;
        case "cancel":
          listType = "canceledDeliveries";
          break;
        default:
          break;
      }

      await axios
        .get(
          `${_baseUrl}/${listType}?customerId=${profile.id}&_sort=date&_order=desc`
        )
        .then((res) => {
          setList(res.data);
        });
    } catch (error) {
      message.error("Something went wrong. please reload!");
    }

    setLoading(false);
  };

  const handleClick = (value) => {
    setActive(value);
    setLoading(true);

    const timeout = setTimeout(() => {
      setLoading(false);
    }, 1000);
  };

  return (
    <>
      <Row style={{ marginBottom: "20px" }} justify="center" align="center">
        <Col className="colContainer" span={8}>
          <p
            onClick={() => handleClick("onGoing")}
            style={{ color: active == "onGoing" ? "orange" : "black" }}
          >
            On Going
          </p>
        </Col>
        <Col className="colContainer" span={8}>
          <p
            onClick={() => handleClick("complete")}
            style={{ color: active == "complete" ? "orange" : "black" }}
          >
            Complete
          </p>
        </Col>
        <Col
          style={{
            alignItems: "center",
            width: " 100%",
            display: "flex",
            justifyContent: "center",
            fontWeight: " 500",
          }}
          span={8}
        >
          <p
            onClick={() => handleClick("cancel")}
            style={{ color: active == "cancel" ? "orange" : "black" }}
          >
            Cancel
          </p>
        </Col>
      </Row>
      <div
        style={{
          display: loading === true ? "flex" : "none",
          width: "100%",
          height: "100px",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        Loading ...
      </div>
      {data.length === 0 ? (
        <div
          style={{
            display: loading === false ? "flex" : "none",
            width: "100%",
            height: "100px",
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          No data
        </div>
      ) : (
        <div
          style={{
            display: loading === false ? "flex" : "none",
            flexDirection: "column",
          }}
        >
          {list.map((item, index) => (
            <div className="itemContainer" key={`product-item-${index}`}>
              <div style={{ display: "flex", flexDirection: "row" }}>
                <img
                  src={item.imageUrl}
                  style={{ height: 90, width: 90, marginRight: "20px" }}
                />
                <div>
                  <p style={{ fontSize: "17px", fontWeight: "500" }}>
                    {item.productName}
                  </p>
                  <p>x {item.quantity}</p>
                </div>
              </div>
              <div style={{ display: "flex", flexDirection: "row" }}>
                <div style={{ marginRight: "50px" }}>
                  P {item.quantity * item.price}
                </div>
                <div style={{ display: active !== "onGoing" ? "none" : "" }}>
                  <Delete data={item} getPurchaseList={getPurchaseList} />
                </div>
              </div>
            </div>
          ))}
        </div>
      )}
    </>
  );
};

export default Purchase;
