import React, { useState } from "react";
import { Button, Modal, message } from "antd";
import {
  CloseOutlined,
  WarningTwoTone,
  DeleteOutlined,
} from "@ant-design/icons";
import { v4 as uuid } from "uuid";
import axios from "axios";
import moment from "moment";

const Delete = ({ data, getPurchaseList }) => {
  const _baseUrl = process.env.REACT_APP_BASE_URL;
  const [visible, setVisible] = useState(false);

  const cancelOngoingDeliveries = async () => {
    try {
      await axios.delete(`${_baseUrl}/ongoingDeliveries/${data.id}`);
      await axios.post(`${_baseUrl}/canceledDeliveries`, {
        ...data,
        id: uuid(),
        date: moment(),
      });
    } catch (error) {
      message.error("Something went wrong. please reload!");
    }
    getPurchaseList();
    setVisible(false);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  return (
    <>
      <DeleteOutlined
        onClick={() => setVisible(true)}
        style={{ fontSize: "18px", cursor: "pointer" }}
      />
      {visible ? (
        <Modal
          visible={true}
          footer={null}
          onCancel={handleCancel}
          closable={false}
        >
          <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "column",
              padding: "30px",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <CloseOutlined
              onClick={handleCancel}
              style={{
                position: "absolute",
                top: "-30px",
                right: "-30px",
                color: "#fff",
              }}
            />
            <WarningTwoTone
              twoToneColor="orange"
              style={{
                fontSize: "50px",
                marginBottom: "10px",
              }}
            />

            <p
              style={{
                fontSize: "18px",
              }}
            >
              Are you sure to Delete this item?
            </p>
            <div
              style={{
                marginTop: "20px",
                width: "250px",
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <Button
                type="primary"
                style={{ width: "100px" }}
                onClick={cancelOngoingDeliveries}
              >
                Confirm
              </Button>
              <Button onClick={handleCancel} style={{ width: "100px" }}>
                Cancel
              </Button>
            </div>
          </div>
        </Modal>
      ) : null}
    </>
  );
};

export default Delete;
