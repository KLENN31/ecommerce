import React, { useState } from "react";
import "./style.css";
import { Formik } from "formik";

import {
  Col,
  Row,
  Select,
  Input,
  Button,
  Form,
  DatePicker,
  message,
} from "antd";
import { EditOutlined, CloseCircleOutlined } from "@ant-design/icons";
import moment from "moment";
import axios from "axios";

const { Option } = Select;

const UserData = ({ profile, updateUser }) => {
  const _baseUrl = process.env.REACT_APP_BASE_URL;
  const [disable, setDisable] = useState(true);
  const [birthDate, setBirthDate] = useState(moment(profile.birthDate));
  const [gender, setGender] = useState(profile.gender);

  const UpdateUserInfo = (values) => {
    try {
      const formData = {
        imageUrl: profile.imageUrl,
        firstName: values.firstName,
        lastName: values.lastName,
        username: values.username,
        role: profile.role,
        password: profile.password,
        address: values.address,
        contact: values.contact,
        age: profile.age,
        birthDate: birthDate,
        gender: gender,
        id: profile.id,
      };
      axios.patch(`${_baseUrl}/users/${profile.id}`, formData);
      updateUser(formData);
      localStorage.setItem("user", JSON.stringify(formData));
    } catch (error) {
      message.error("Something went wrong. please reload!!");
    }
  };

  return (
    <>
      <div
        style={{
          width: "100%",
          display: "flex",
          justifyContent: "flex-end",
          marginBottom: "20px",
        }}
      >
        {disable === true ? (
          <EditOutlined
            style={{ fontSize: "17px" }}
            onClick={() => setDisable(false)}
          />
        ) : (
          <CloseCircleOutlined
            style={{ fontSize: "17px" }}
            onClick={() => setDisable(true)}
          />
        )}
      </div>

      {/* Form Component */}
      <Formik
        initialValues={{
          username: profile.username,
          firstName: profile.firstName,
          lastName: profile.lastName,
          address: profile.address,
          gender: profile.gender,
          contact: profile.contact,
          birthDate: moment(profile.birthDate),
        }}
        validate={(values) => {
          const errors = {};
          if (!values.username) {
            errors.username = "Required";
          } else if (
            !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.username)
          ) {
            errors.username = "Invalid username address";
          }
          return errors;
        }}
        onSubmit={(values) => {
          UpdateUserInfo(values);
          setDisable(true);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
        }) => (
          <Form onSubmit={handleSubmit}>
            <Input
              name="firstName"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.firstName}
              placeholder="firstName"
              disabled={disable}
            />
            <div style={{ minHeight: "20px", minWidth: "100%" }}></div>
            <Input
              name="lastName"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.lastName}
              placeholder="lastName"
              disabled={disable}
            />
            <div style={{ minHeight: "20px", minWidth: "100%" }}></div>
            <Input
              name="address"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.address}
              placeholder="address"
              disabled={disable}
            />
            <div style={{ minHeight: "20px", minWidth: "100%" }}></div>
            <Input
              type="email"
              name="username"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.username}
              placeholder="username"
              disabled={disable}
            />
            <div style={{ minHeight: "20px", minWidth: "100%" }}>
              <small style={{ color: "red" }}>
                {errors.username && touched.username && errors.username}
              </small>
            </div>

            <Input
              name="contact"
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.contact}
              placeholder="contact"
              disabled={disable}
            />
            <div style={{ minHeight: "20px", minWidth: "100%" }}></div>
            <Row gutter={12} style={{ width: "100" }}>
              <Col span={12}>
                <DatePicker
                  name="birthDate"
                  defaultValue={values.birthDate}
                  placeholder="Select Date"
                  style={{ width: "100%" }}
                  disabled={disable}
                  onChange={(value) => setBirthDate(value)}
                />
              </Col>
              <Col span={12}>
                <Select
                  name="gender"
                  style={{ width: "100%" }}
                  placeholder="Gender"
                  disabled={disable}
                  defaultValue={values.gender}
                  onChange={(value) => setGender(value)}
                >
                  <Option value="Male">Male</Option>
                  <Option value="Female">Female</Option>
                  <Option value="Rather not say">Rather not say</Option>
                </Select>
              </Col>
            </Row>

            <div style={{ minHeight: "20px", minWidth: "100%" }} />

            <div
              style={{
                width: "100%",
                display: "flex",
                justifyContent: "flex-end",
              }}
            >
              <Button
                style={{ width: "100px" }}
                type="primary"
                htmlType="submit"
                onClick={handleSubmit}
                disabled={disable}
              >
                Submit
              </Button>
            </div>
          </Form>
        )}
      </Formik>
      {/* </Col>
      </Row> */}
    </>
  );
};

export default UserData;
