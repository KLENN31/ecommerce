import React from "react";
import { useParams } from "react-router-dom";
import "./style.css";
import { Divider } from "antd";
import UserData from "./form";
import Purchase from "./purchase";

const Profile = (props) => {
  let {} = useParams();

  return (
    <div className="container">
      <div className="content-container">
        <UserData {...props} />
        <Divider />
        <Purchase {...props} />
      </div>
    </div>
  );
};

export default Profile;
