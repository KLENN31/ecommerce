import React, { useEffect, useState } from "react";
import { Button, Modal, Divider } from "antd";
import { CloseOutlined } from "@ant-design/icons";
import moment from "moment";

const Summary = ({ start, end, data }) => {
  const [visible, setVisible] = useState(false);
  const [listSummary, setListSummary] = useState([]);
  const [totalSale, setTotalSale] = useState(0);

  const showModal = () => {
    setVisible(true);
  };

  const handleOk = () => {
    setVisible(false);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  useEffect(() => {
    if (data.length != 0) {
      let list = {};
      let total = 0.0;
      let newList = [];

      data.forEach((item) => {
        let quantity = 0;
        if (list[item.productName] != undefined) {
          quantity = item.quantity + list[item.productName].quantity;
        } else {
          quantity = item.quantity;
        }
        list[item.productName] = {
          quantity: quantity,
          total: quantity * item.price,
        };
      });

      Object.keys(list).forEach((key, index) => {
        total = list[key].total + total;
        newList.push({
          productName: key,
          total: list[key].total,
          quantity: list[key].quantity,
        });
      });
      setListSummary(newList);
      setTotalSale(total);
    }
  }, [data]);

  return (
    <>
      <Button
        disabled={start == null || end == null || data.length == 0}
        onClick={() => {
          setVisible(true);
        }}
        style={{ width: "100px" }}
        type="primary"
      >
        Summary
      </Button>
      {visible ? (
        <Modal
          visible={true}
          footer={null}
          // onOk={handleOk}
          onCancel={handleCancel}
          closable={false}
        >
          <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "column",
              padding: "30px",
            }}
          >
            <CloseOutlined
              onClick={handleCancel}
              style={{
                position: "absolute",
                top: "-30px",
                right: "-30px",
                color: "#fff",
              }}
            />
            <h1>
              Sales of {moment(start).format("ll")} - {moment(end).format("ll")}
            </h1>
            <ul>
              {listSummary.map((item, index) => (
                <li
                  style={{ display: "flex", flexDirection: "row" }}
                  key={`product-list-${index}`}
                >
                  <div
                    style={{
                      width: "90%",
                      display: "flex",
                      flexDirection: "row",
                    }}
                  >
                    <p>x{item.quantity}</p>
                    <b style={{ marginLeft: "10px" }}>{item.productName}</b>
                  </div>

                  <p>P{item.total}</p>
                </li>
              ))}
            </ul>
            <Divider />
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "space-between",
              }}
            >
              <b>Total</b>
              <b>P {totalSale}</b>
            </div>
          </div>
        </Modal>
      ) : null}
    </>
  );
};

export default Summary;
