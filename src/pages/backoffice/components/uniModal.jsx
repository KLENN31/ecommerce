import React from "react";
import { Button, Modal } from "antd";
import { WarningTwoTone, QuestionCircleTwoTone } from "@ant-design/icons";

const UniModal = ({ visible, active, handleCancel, handleConfirm }) => {
  return (
    <>
      <Modal
        visible={visible}
        footer={null}
        onCancel={handleCancel}
        closable={false}
      >
        <div
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            alignItems: "center",
            padding: "30px",
          }}
        >
          {active === "product" ? (
            <WarningTwoTone
              twoToneColor="orange"
              style={{
                fontSize: "50px",
                marginBottom: "10px",
              }}
            />
          ) : (
            <QuestionCircleTwoTone
              twoToneColor="#52c41a"
              style={{
                fontSize: "50px",
                marginBottom: "10px",
              }}
            />
          )}

          <p
            style={{
              display: active === "onGoing" ? "flex" : "none",
              fontSize: "18px",
            }}
          >
            Set as Delivered?
          </p>
          <p
            style={{
              display: active === "product" ? "flex" : "none",
              fontSize: "18px",
            }}
          >
            Are you sure to Delete this item?
          </p>
          <div
            style={{
              marginTop: "20px",
              width: "250px",
              display: "flex",
              flexDirection: "row",
              justifyContent: "space-between",
            }}
          >
            <Button
              type="primary"
              style={{ width: "100px" }}
              onClick={handleConfirm}
            >
              Confirm
            </Button>
            <Button onClick={handleCancel} style={{ width: "100px" }}>
              Cancel
            </Button>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default UniModal;
