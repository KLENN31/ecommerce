import React from "react";
import logo from "./favicon.png";
import { useHistory } from "react-router-dom";

const Sidebar = ({ handleClick, active, handleData, setUserInfo }) => {
  const history = useHistory();
  return (
    <div className="sidebar-container">
      <div
        style={{
          backgroundImage: `url(${logo})`,
          backgroundRepeat: "no-repeat",
          backgroundPosition: `center`,
          backgroundSize: "contain",
          width: "100%",
          height: "100px",
        }}
      />
      <ul style={{ height: "80%" }}>
        <li key={"menu-01"}>
          <a
            onClick={() => {
              handleClick("sales");
              handleData("deliveredProduct");
            }}
            style={{ color: active === "sales" ? "orange" : "#fff" }}
          >
            Sales
          </a>
        </li>
        <li key={"menu-02"}>
          <a
            onClick={() => {
              handleClick("onGoing");
              handleData("ongoingDeliveries");
            }}
            style={{ color: active === "onGoing" ? "orange" : "#fff" }}
          >
            On Going
          </a>
        </li>
        <li key={"menu-03"}>
          <a
            onClick={() => {
              handleClick("canceled");
              handleData("canceledDeliveries");
            }}
            style={{ color: active === "canceled" ? "orange" : "#fff" }}
          >
            Canceled
          </a>
        </li>
        <li key={"menu-04"}>
          <a
            onClick={() => {
              handleClick("product");
              handleData("product");
            }}
            style={{ color: active === "product" ? "orange" : "#fff" }}
          >
            Products
          </a>
        </li>
      </ul>
      <div
        onClick={() => {
          setUserInfo(null);
          localStorage.clear();
          history.push("/signin");
        }}
        style={{ color: "#fff", cursor: "pointer", fontSize: 18 }}
      >
        Log Out
      </div>
    </div>
  );
};

export default Sidebar;
