import React, { useState } from "react";
import { Space, Table, Button, DatePicker, message } from "antd";
import UniModal from "./uniModal";
import moment from "moment";
import axios from "axios";
import Summary from "./summary";
import NewProduct from "./addProduct";
import { v4 as uuid } from "uuid";
import CommonModal from "../../../common/modal";

const { RangePicker } = DatePicker;

const BackOfficeTable = ({ data, active, setDateRange, getTableData }) => {
  const _baseUrl = process.env.REACT_APP_BASE_URL;
  const [visible, setVisible] = useState(false);
  const [product, setProduct] = useState(null);
  const [start, setStart] = useState(null);
  const [end, setEnd] = useState(null);
  const [record, setRecord] = useState(null);
  const [view, setView] = useState(false);

  const handleCancel = () => {
    setVisible(false);
    setView(false);
  };

  const handleConfirm = () => {
    switch (active) {
      case "onGoing":
        deliveredProduct();
        break;
      case "product":
        removeProduct();
        break;
      default:
        break;
    }
  };

  const deliveredProduct = async () => {
    try {
      await axios.delete(`${_baseUrl}/ongoingDeliveries/${product.id}`);
      await axios.post(`${_baseUrl}/deliveredProduct`, {
        ...product,
        date: moment().format(),
        id: uuid(),
      });
      await getTableData("ongoingDeliveries");
    } catch (error) {
      message.error("Something went wrong please reload!");
      await getTableData("ongoingDeliveries");
    }
    setVisible(false);
  };

  const removeProduct = async () => {
    try {
      await axios.delete(`${_baseUrl}/product/${product.id}`);
      await getTableData("product");
    } catch (error) {
      message.error("Something went wrong please reload!");
      await getTableData("product");
    }
    setVisible(false);
  };

  const handleViewProduct = (record) => {
    setRecord(record);
    setView(true);
  };

  const columns = [
    {
      title: "Date",
      key: "date",
      render: (_, record) => <p>{moment(record.date).format("L")}</p>,
    },
    {
      title: "Time",
      key: "time",
      render: (_, record) => <p>{moment(record.date).format("LT")}</p>,
    },
    {
      title: "Name",
      dataIndex: "productName",
      key: "productName",
    },
    {
      title: "Quantity",
      dataIndex: "quantity",
      key: "quantity",
    },

    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },
    {
      title: "Total",
      key: "total",
      render: (_, record) => <p>{record.quantity * record.price}</p>,
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          {active == "onGoing" ? (
            <Button
              onClick={() => {
                setVisible(true);
                setProduct(record);
              }}
              style={{ width: "100px", backgroundColor: "#4CAF50" }}
              type="primary"
            >
              Delivered
            </Button>
          ) : (
            <></>
          )}
          <Button
            onClick={() => handleViewProduct(record)}
            style={{ width: "100px" }}
            type="primary"
          >
            Preview
          </Button>
        </Space>
      ),
    },
  ];

  const inverntoryColumn = [
    {
      title: "Image",
      dataIndex: "imageUrl",
      key: "imageUrl",
      render: (_, record) => (
        <div
          style={{
            backgroundImage: `url(${record.imageUrl})`,
            width: "50px",
            height: "50px",
            backgroundRepeat: "no-repeat",
            backgroundPosition: "center center",
            backgroundSize: "contain",
          }}
        />
      ),
    },
    {
      title: "Name",
      dataIndex: "productName",
      key: "productName",
    },
    // {
    //   title: "Quantity",
    //   dataIndex: "quantity",
    //   key: "quantity",
    // },

    {
      title: "Price",
      dataIndex: "price",
      key: "price",
    },

    {
      title: "Category",
      dataIndex: "category",
      key: "category",
    },
    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <NewProduct
            ButtonLabel={"Edit"}
            initialValue={record}
            formType={"edit"}
            getTableData={getTableData}
          />
          <Button
            onClick={() => {
              setVisible(true);
              setProduct(record);
            }}
            style={{ width: "120px" }}
            type="danger"
          >
            Delete
          </Button>
        </Space>
      ),
    },
  ];

  const handleDate = (e) => {
    if (e != null) {
      setDateRange({
        startDate: moment(e[0]).format(),
        endDate: moment(e[1]).format(),
      });
      setStart(e[0]);
      setEnd(e[1]);
    } else {
      setDateRange(null);
      setStart(null);
      setEnd(null);
    }
  };

  return (
    <div>
      <div
        style={{
          marginBottom: "20px",
          display: active == "sales" ? "flex" : "none",
          flexDirection: "row",
          justifyContent: "space-between",
        }}
      >
        <RangePicker onChange={(e) => handleDate(e)} />
        <Summary start={start} end={end} data={data} />
        <div style={{ display: active == "product" ? "flex" : "none" }}></div>
      </div>
      <div
        style={{
          marginBottom: "20px",
          display: active == "product" ? "flex" : "none",
        }}
      >
        <NewProduct
          ButtonLabel={"New Product"}
          formType={"add"}
          getTableData={getTableData}
        />
      </div>
      <Table
        columns={active === "product" ? inverntoryColumn : columns}
        dataSource={data}
        pagination={false}
      />

      <UniModal
        handleCancel={handleCancel}
        active={active}
        visible={visible}
        handleConfirm={handleConfirm}
      />

      <CommonModal record={record} view={view} handleCancel={handleCancel} />
    </div>
  );
};

export default BackOfficeTable;
