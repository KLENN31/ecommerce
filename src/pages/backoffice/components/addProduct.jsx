import React, { useEffect, useState } from "react";
import { Button, Modal, message, Input, Form, Select } from "antd";
import { CloseOutlined } from "@ant-design/icons";
import { Formik } from "formik";
import axios from "axios";
import { v4 as uuid } from "uuid";

const { Option } = Select;

const NewProduct = ({ initialValue, ButtonLabel, formType, getTableData }) => {
  const _baseUrl = process.env.REACT_APP_BASE_URL;
  const [visible, setVisible] = useState(false);
  const [category, setCategory] = useState(null);

  useEffect(() => {
    if (formType == "edit") {
      setCategory(initialValue.category);
    } else {
      setCategory(null);
    }
  }, [initialValue]);

  const showModal = () => {
    setVisible(true);
  };
  const handleCategory = (e) => {
    setCategory(e);
  };

  const handleSubmit = async (values) => {
    try {
      if (formType == "add") {
        await axios.post(`${_baseUrl}/product`, {
          id: uuid(),
          productName: values.productName,
          imageUrl: values.imageUrl,
          description: values.description,
          price: values.price,
          category: category,
        });
      } else if (formType == "edit") {
        const id = initialValue.id;
        await axios.patch(`${_baseUrl}/product/${id}`, {
          productName: values.productName,
          imageUrl: values.imageUrl,
          description: values.description,
          price: values.price,
          category: category,
        });
      }
      await getTableData("product");
    } catch (error) {
      message.error("Something went wrong. please reload!");
      await getTableData("product");
    }

    setVisible(false);
  };

  const handleCancel = () => {
    setVisible(false);
  };

  return (
    <>
      <Button onClick={showModal} style={{ width: "120px" }} type="primary">
        {ButtonLabel}
      </Button>
      {visible ? (
        <Modal
          style={{ width: "500px" }}
          visible={true}
          footer={null}
          onCancel={handleCancel}
          closable={false}
        >
          <div
            style={{
              width: "100%",
              display: "flex",
              flexDirection: "column",
              padding: "30px",
            }}
          >
            <CloseOutlined
              onClick={handleCancel}
              style={{
                position: "absolute",
                top: "-30px",
                right: "-30px",
                color: "#fff",
              }}
            />
            <Formik
              initialValues={
                initialValue ?? {
                  productName: "",
                  description: "",
                  price: "",
                  category: "",
                  imgUrl: "",
                }
              }
              onSubmit={(values) => handleSubmit(values)}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
              }) => (
                <Form onSubmit={handleSubmit} initialValues={initialValue}>
                  <Input
                    name="imageUrl"
                    placeholder="Image URL"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.imageUrl}
                  />
                  <div style={{ minHeight: "20px", minWidth: "100%" }}>
                    <small style={{ color: "red" }}>
                      {errors.imgUrl && touched.imgUrl && errors.imgUrl}
                    </small>
                  </div>
                  <Input
                    type="productName"
                    name="productName"
                    placeholder="Product Name"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.productName}
                  />
                  <div style={{ minHeight: "20px", minWidth: "100%" }}>
                    <small style={{ color: "red" }}>
                      {errors.productName &&
                        touched.productName &&
                        errors.productName}
                    </small>
                  </div>
                  <Input
                    name="description"
                    placeholder="Description"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.description}
                  />
                  <div style={{ minHeight: "20px", minWidth: "100%" }}>
                    <small style={{ color: "red" }}>
                      {errors.description &&
                        touched.description &&
                        errors.description}
                    </small>
                  </div>
                  <Input
                    name="price"
                    placeholder="Price"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.price}
                  />
                  <div style={{ minHeight: "20px", minWidth: "100%" }}>
                    <small style={{ color: "red" }}>
                      {errors.price && touched.price && errors.price}
                    </small>
                  </div>
                  <Select
                    value={category}
                    name="category"
                    placeholder="Category"
                    style={{ width: "100%" }}
                    onChange={(e) => handleCategory(e)}
                  >
                    <Option value="shrip">Shrimp</Option>
                    <Option value="fish">Fish</Option>
                    <Option value="squid">Squid</Option>
                    <Option value="shellfish">Shellfish</Option>
                  </Select>
                  <div style={{ minHeight: "20px", minWidth: "100%" }}>
                    <small style={{ color: "red" }}>
                      {errors.category && touched.category && errors.category}
                    </small>
                  </div>
                  <Button
                    style={{ width: "100px", alignSelf: "center" }}
                    type="primary"
                    htmlType="submit"
                    onClick={handleSubmit}
                  >
                    Submit
                  </Button>
                </Form>
              )}
            </Formik>
          </div>
        </Modal>
      ) : null}
    </>
  );
};

export default NewProduct;
