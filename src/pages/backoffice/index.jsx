import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import "./style.css";
import Sidebar from "./components/sidebar";
import BackOfficeTable from "./components/table";
import axios from "axios";
import { message } from "antd";

const BackOffice = ({ setUserInfo }) => {
  const _baseUrl = process.env.REACT_APP_BASE_URL;
  const [active, setActive] = useState("sales");
  const [list, setList] = useState([]);
  const [dateRange, setDateRange] = useState(null);

  const handleClick = (value) => {
    setActive(value);
  };

  const getTableData = (value) => {
    try {
      let params = `_sort=date&_order=desc`;
      if (value === "deliveredProduct" && dateRange != null) {
        params = `date_gte=${dateRange.startDate}&date_lte=${dateRange.endDate}&_sort=date&_order=desc`;
      }
      axios.get(`${_baseUrl}/${value}?${params}`).then((res) => {
        setList(res.data);
      });
    } catch (error) {
      message.error("Something went wrong. please reload!");
    }
  };

  const renderState = () => {
    switch (active) {
      case "sales":
        return "Sales";
      case "onGoing":
        return "On Going";
      case "canceled":
        return "Canceled";
      default:
        return "Products";
    }
  };

  let {} = useParams();
  useEffect(() => {
    if (active == "sales") {
      getTableData("deliveredProduct");
    }
  }, [dateRange]);

  return (
    <div className="containerBackOffice">
      <Sidebar
        setUserInfo={setUserInfo}
        handleClick={handleClick}
        active={active}
        handleData={getTableData}
      />
      <div className="containerBackOffice__content">
        <div className="backOffice_header">
          <p>{renderState()}</p>
        </div>

        <div className="backOffice_content">
          <BackOfficeTable
            data={list}
            active={active}
            setDateRange={setDateRange}
            getTableData={getTableData}
          />
        </div>
      </div>
    </div>
  );
};

export default BackOffice;
