import React, { useState } from "react";
import { Formik } from "formik";
import { Button, Input, Form, Row, Col, Select, DatePicker } from "antd";
import logo from "./backoffice/components/favicon.png";
import axios from "axios";
import { useHistory } from "react-router-dom";

import { v4 as uuid } from "uuid";
const { Option } = Select;

const Signup = () => {
  const _baseUrl = process.env.REACT_APP_BASE_URL;
  const [date, setDate] = useState("");
  const [gender, setGender] = useState("");
  const [role, setRole] = useState("");

  const history = useHistory();

  const handleDate = (e) => {
    setDate(e._d);
  };
  const handleRole = (e) => {
    setRole(e);
  };
  const handleGender = (e) => {
    setGender(e);
  };

  const handleSubmit = async (value) => {
    await axios.post(`${_baseUrl}/users`, {
      imageUrl: "",
      firstName: value.firstName,
      lastName: value.lastName,
      username: value.email,
      role: role,
      password: value.password,
      address: value.address,
      contact: value.contact,
      age: value.age,
      birthDate: date,
      gender: gender,
      id: uuid(),
    });
    history.push("/signin");
  };

  return (
    <div
      style={{
        width: "100vw",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div
        style={{
          border: "1px solid #888",
          height: "auto",
          width: "400px",
          padding: "30px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          backgroundColor: "#fff",
          borderRadius: "3px",
        }}
      >
        <div
          style={{
            width: "200px",
            height: "200px",
            backgroundImage: `url(${logo})`,
            backgroundSize: "contain",
            backgroundRepeat: "no-repeat",
          }}
        />

        {/* Form Component */}
        <Formik
          style={{
            width: "100%",
            backgroundColor: "red",
          }}
          initialValues={{ email: "", password: "" }}
          validate={(values) => {
            const errors = {};
            if (!values.email) {
              errors.email = "Required";
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = "Invalid email address";
            }
            return errors;
          }}
          onSubmit={(values) => handleSubmit(values)}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <Form
              style={{
                width: "100%",
              }}
              onSubmit={handleSubmit}
            >
              <Row gutter={8}>
                <Col span={12}>
                  <Input
                    placeholder="First Name"
                    type="firstName"
                    name="firstName"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.firstName}
                  />
                </Col>
                <Col span={12}>
                  <Input
                    placeholder="Last Name"
                    type="lastName"
                    name="lastName"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.lastName}
                  />
                </Col>
              </Row>
              {/* <div style={{ minHeight: "20px", minWidth: "100%" }}>
                <small style={{ color: "red" }}></small>
              </div>
              <Input
                placeholder="Image URL"
                type="imageUrl"
                name="imageUrl"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.imageUrl}
              /> */}
              <div style={{ minHeight: "20px", minWidth: "100%" }}></div>
              <Input
                placeholder="Email"
                type="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              <div style={{ minHeight: "20px", minWidth: "100%" }}>
                <small style={{ color: "red" }}>
                  {errors.username && touched.username && errors.username}
                </small>
              </div>
              <Input
                placeholder="Password"
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              <div style={{ minHeight: "20px", minWidth: "100%" }}>
                <small style={{ color: "red" }}>
                  {errors.password && touched.password && errors.password}
                </small>
              </div>

              <Input
                placeholder="Address"
                type="address"
                name="address"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.address}
              />
              <div style={{ minHeight: "20px", minWidth: "100%" }}>
                <small style={{ color: "red" }}>
                  {errors.address && touched.address && errors.address}
                </small>
              </div>
              <Row gutter={8}>
                <Col span={12}>
                  <Input
                    placeholder="Age"
                    type="number"
                    name="age"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.age}
                  />
                </Col>
                <Col span={12}>
                  <DatePicker
                    placeholder="Birth Date"
                    style={{ width: "100%" }}
                    type="birthDate"
                    name="birthDate"
                    onChange={(e) => handleDate(e)}
                    onBlur={handleBlur}
                    value={values.birthDate}
                  />
                </Col>
              </Row>
              <div style={{ minHeight: "20px", minWidth: "100%" }}></div>
              <Row gutter={8}>
                <Col span={12}>
                  <Input
                    placeholder="Contact"
                    type="number"
                    name="contact"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.contact}
                  />
                </Col>
                <Col span={12}>
                  <Select
                    name="gender"
                    placeholder="Select gender"
                    style={{ width: "100%" }}
                    onChange={(e) => handleGender(e)}
                  >
                    <Option value="male">Male</Option>
                    <Option value="female">Female</Option>
                  </Select>
                </Col>
              </Row>
              <div style={{ minHeight: "20px", minWidth: "100%" }}></div>
              <Select
                name="role"
                placeholder="Select Role"
                style={{ width: "100%" }}
                onChange={(e) => handleRole(e)}
              >
                <Option value="customer">Customer</Option>
                <Option value="employee">Delivery driver</Option>
              </Select>
              <div style={{ minHeight: "20px", minWidth: "100%" }}></div>

              <Button
                style={{ width: "100%", alignSelf: "center" }}
                type="primary"
                htmlType="submit"
                onClick={() => handleSubmit(values)}
              >
                Submit
              </Button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default Signup;
