import React from "react";
import { Formik } from "formik";
import { Button, Input, Form, message } from "antd";
import logo from "./backoffice/components/favicon.png";

import axios from "axios";
import { useHistory } from "react-router-dom";

const Signin = ({ setUserInfo }) => {
  const _baseUrl = process.env.REACT_APP_BASE_URL;
  const history = useHistory();

  const handleSubmit = async ({ email, password }) => {
    if (
      email !== undefined &&
      password !== undefined &&
      email !== "" &&
      password !== ""
    ) {
      const response = await axios.get(
        `${_baseUrl}/users?username=${email}&password=${password}`
      );
      if (response.data.length == 0) {
        message.error("Invalid Username or Password");
      } else if (response.data.length == 1) {
        const data = response.data[0];
        setUserInfo(data);
        localStorage.setItem("user", JSON.stringify(data));
        if (data.role == "employee") {
          history.push("/back-office");
        } else if (data.role == "customer") {
          history.push("/market");
        }
      }
    }
  };
  return (
    <div
      style={{
        width: "100vw",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <div
        style={{
          border: "1px solid #888",
          height: "auto",
          width: "400px",
          padding: "30px",
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          flexDirection: "column",
          backgroundColor: "#fff",
          borderRadius: "3px",
        }}
      >
        <div
          style={{
            width: "200px",
            height: "200px",
            backgroundImage: `url(${logo})`,
            backgroundSize: "contain",
            backgroundRepeat: "no-repeat",
          }}
        />
        <Formik
          style={{
            width: "100%",
            backgroundColor: "red",
          }}
          initialValues={{ email: "", password: "" }}
          validate={(values) => {
            const errors = {};
            if (!values.email) {
              errors.email = "Required";
            } else if (
              !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
            ) {
              errors.email = "Invalid email address";
            }
            return errors;
          }}
          onSubmit={(values) => handleSubmit(values)}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
          }) => (
            <Form
              style={{
                width: "100%",
              }}
              onSubmit={handleSubmit}
            >
              <Input
                required
                placeholder="Email"
                type="email"
                name="email"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
              <div style={{ minHeight: "20px", minWidth: "100%" }}>
                <small style={{ color: "red" }}>
                  {errors.username && touched.username && errors.username}
                </small>
              </div>
              <Input
                required
                placeholder="Password"
                type="password"
                name="password"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
              <div
                style={{
                  minHeight: "40px",
                  minWidth: "100%",
                  display: "flex",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <a href="/signup">
                  <small style={{ color: "#888" }}>Sign up</small>
                </a>
              </div>
              <Button
                style={{ width: "100%", alignSelf: "center" }}
                type="primary"
                htmlType="submit"
                onClick={() => handleSubmit(values)}
              >
                Submit
              </Button>
            </Form>
          )}
        </Formik>
      </div>
    </div>
  );
};

export default Signin;
