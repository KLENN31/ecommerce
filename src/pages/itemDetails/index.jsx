import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import "./style.css";
import { Col, message, Row } from "antd";
import axios from "axios";

const ItemDetails = ({ addToCart }) => {
  const _baseUrl = process.env.REACT_APP_BASE_URL;
  const [productInfo, setProductInfo] = useState({
    id: "",
    productName: "",
    imageUrl: "",
    description: "",
    quantity: 0,
    price: 0,
    category: "",
  });

  let { id } = useParams();

  useEffect(() => {
    try {
      // Request from Database Item product details
      axios
        .get(`${_baseUrl}/product/${id}`)
        .then((res) => setProductInfo(res.data));
    } catch (error) {
      message.error("Something went wrong. please reload!");
    }
  }, []);

  return (
    <div className="container">
      <div className="content-container">
        <Row style={{ width: "100%", height: "100%" }} gutter={24}>
          <Col lg={{ span: 9 }}>
            <div
              className="image-container"
              style={{
                backgroundImage: `url(${productInfo.imageUrl})`,
              }}
            />
          </Col>
          <Col lg={{ span: 15 }}>
            <div
              style={{
                height: "100%",
                width: "100%",
                position: "relative",
              }}
            >
              <p className="title">{productInfo.productName}</p>
              <div className="price">₱{productInfo.price}</div>
              <h4>Description</h4>
              <p style={{ marginBottom: "30px", color: "#888" }}>
                {productInfo.description}
              </p>

              <div
                style={{
                  backgroundColor: "#fff !important",
                  position: "absolute",
                  bottom: "0px",
                  right: "0px",
                }}
              >
                <button onClick={() => addToCart(productInfo)}>
                  <i className="fa fa-plus"></i>
                </button>
              </div>
            </div>
          </Col>
        </Row>
      </div>
    </div>
  );
};

export default ItemDetails;
